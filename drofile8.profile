<?php

/**
 * @file
 * Enables modules and site configuration for the Drofile8 profile.
 */

use Drupal\contact\Entity\ContactForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function drofile8_form_install_configure_form_alter(&$form, FormStateInterface $form_state) {
  // Pre-populate the site name and email address.
  $form['site_information']['site_name']['#default_value'] = $_SERVER['SERVER_NAME'];
  $form['site_information']['site_mail']['#default_value'] = 'dp_admin@mobomo.com';

  // Account information defaults.
  $form['admin_account']['account']['name']['#default_value'] = 'dp_admin';
  $form['admin_account']['account']['mail']['#default_value'] = 'dp_admin@mobomo.com';

  // Date/time settings.
  $form['regional_settings']['site_default_country']['#default_value'] = 'US';
  $form['regional_settings']['date_default_timezone']['#default_value'] = 'America/New_York';

  // Unset the timezone detect thingy.
  unset($form['server_settings']['date_default_timezone']['#attributes']['class']);

  // Only check for updates, no need for email notifications.
  $form['update_notifications']['enable_update_status_emails']['#default_value'] = 0;
}

/**
 * Implements hook_install_tasks().
 */
function drofile8_install_tasks() {
  return [
    'drofile8_install_content' => [
      'display_name' => t('Install Content'),
      'type' => 'normal',
    ],
  ];
}

/**
 * Install task callback. Adds default content.
 *
 * @param array $install_state
 *   The current install state.
 */
function drofile8_install_content(array &$install_state) {
  // Create node object with attached file.
  $node = Node::create([
    'type'        => 'homepage',
    'title'       => 'Homepage',
    'body' => [
      'value' => _homepage_content(),
      'format' => 'basic_html',
    ],
  ]);
  $node->save();
}

/**
 * Content callback for eureka_install_content().
 *
 * @return string
 *   Content text for the homepage.
 */
function _homepage_content() {
  return '<h2>Welcome to Drupal! Drofile8 profile :)</h2>' .
    '<p>Drofile 8 is a basic Drupal 8 profile installation aiming to simplify and automate really annoying and repetitive tasks for Drupal installation and configuration.</p>' .
    '<p>Hopefully I will have some spare time to make this profile grow and cover all required tasks :)</p>';
}
